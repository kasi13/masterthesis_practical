#ifndef WEB_CRAWLER
#define WEB_CRAWLER

#ifndef _FSTREAM_
#include <fstream>
#endif


namespace webcrawler {

	static std::string GrabRandomWikipediaArticle_ToString(std::string callSize = "1048576")
	{

		std::string init = "py webCrawler.py " + callSize;

		const char* call = init.c_str();
		//i know this method is dirty, but it works pretty well :)
		system(call); //<-- calling a python script to call the wikipedia random link (https://en.wikipedia.org/wiki/Special:Random) and saving the article body from the editing page into temp.txt
		std::cout << "Grabbed a text from wikipedia size: ";

		std::streampos size;
		char* memblock;

		std::ifstream file("temp.txt", std::ios::in | std::ios::ate);
		if (file.is_open())
		{
			size = file.tellg();
			std::cout << size << std::endl;
			memblock = new char[size];
			file.seekg(0, std::ios::beg);
			file.read(memblock, size);
			file.close();


			std::string s = memblock;
			delete[] memblock;
			return s;
		}
		return "NULL";
	}

	static void GrabRandomWikipediaArticle_ToFile(std::string callSize = "1048576")
	{

		std::string init = "py webCrawler.py " + callSize;

		const char* call = init.c_str();

		//std::cout << "Grabbed a text from wikipedia size: ";
		//i know this method is dirty, but it works pretty well :)
		system(call); //<-- calling a python script to call the wikipedia random link (https://en.wikipedia.org/wiki/Special:Random) and saving the article body from the editing page into temp.txt
		
		/*
		std::streampos size;
		char* memblock;

		std::ifstream file("temp.txt", std::ios::in | std::ios::ate);
		if (file.is_open())
		{
			size = file.tellg();
			std::cout << size << std::endl;
			file.close();
		}*/
	}
}
#endif
