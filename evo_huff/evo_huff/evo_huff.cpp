#include <iostream>

//huffman
extern "C" {
#include "huffman.h"
}

//galib
#include <ga/ga.h>

//webcrawler
#include "webCrawler.h"

#ifndef  _IOSTREAM_  
#include <iostream>
#endif


#ifndef _FILESYSTEM_
#include <filesystem>
#endif

#ifndef _FSTREAM_
#include <fstream>
#endif


#if _WIN32 | _WIN64

#define OPEN_IN(HANDLE, PATH, OFLAGS)_sopen_s(&HANDLE, PATH, OFLAGS, _SH_DENYNO, 0)
#define OPEN_OUT(HANDLE, PATH, OFLAGS)_sopen_s(&HANDLE, PATH, OFLAGS, _SH_DENYNO, _S_IWRITE)

#include <direct.h>
#include <io.h>
#include <windows.h>
#else
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>

#define _O_WRONLY O_WRONLY
#define _O_BINARY 0x0000
#define _O_CREAT O_CREAT
#define _O_RDONLY O_RDONLY
#define _O_TRUNC O_TRUNC
#define _O_TEMPORARY 0x0400

#define OPEN_IN(HANDLE, PATH, OFLAGS)(HANDLE = open(PATH, OFLAGS, S_IRUSR | S_IWUSR)) < 0
#define OPEN_OUT(HANDLE, PATH, OFLAGS)(HANDLE = open(PATH, OFLAGS, S_IRUSR | S_IWUSR)) < 0
#endif


#include <fcntl.h>
#include <thread>
#include <chrono>
#include <string>
#include <vector>

#undef min
#undef max

#define UINT8_T 256
#define UINT16_T 65536

#define GA_LIMIT UINT16_T

float Objective(GAGenome& g);
void printCommands(std::string msg = "Missing input");
void Initalizer(GAGenome& c);
static std::filesystem::path get_current_dir();
void ga_compress(int pop, int gen, int size, float mut, float limit, int rinDelay, bool rin, bool exp);
void ga_compress_auto();
void ExportGenome(const GAGenome& g);
void Benchmark(std::string filePath, std::string hist);

#if _WIN32 | _WIN64
    std::string openfilename(LPWSTR filter = (LPWSTR)(L"*.*\0*.*\0"), HWND owner = NULL) {
        OPENFILENAME ofn;
        LPWSTR fileName = reinterpret_cast<LPWSTR>(calloc(MAX_PATH, sizeof(LPWSTR)));
        ZeroMemory(&ofn, sizeof(ofn));

        ofn.lStructSize = sizeof(OPENFILENAME);
        ofn.hwndOwner = owner;
        ofn.lpstrFilter = filter;
        ofn.lpstrFile = fileName;
        ofn.nMaxFile = MAX_PATH;
        ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
        ofn.lpstrDefExt = (LPWSTR)(L"");

        std::string fileNameStr = "";

        if (GetOpenFileName(&ofn))
        {
            for (int i = 0; i < MAX_PATH && fileName[i] != NULL; ++i)
            {
                fileNameStr += fileName[i];
            }

            return fileNameStr;
        }
        else
        {
            return "";
        }
    }
#endif

void Huffman_Encode_Ga(std::string filePath = "", std::string histPath = "", std::string outPath = "encoded.huf");
void Huffman_Decode_Ga(std::string inPath, std::string outPath);

int genomeCounter = 0;
int generationCounter = 0;

int in, out;

std::string hist_in;
std::string trainingPath = "./pop/";//"C:\\GA_Huffman_Training\\population";

GATreeGenome<std::pair<bool, int16_t>> debugTree;


bool run = true;
bool save = false;
bool g_rin = false;

enum Mode {swap, incdecins};

Mode g_mode = incdecins;

float g_smut = 0.5f;
float g_pmut = 0.001f;

void input()
{
    std::string input;

    const std::string stp_msg = "Stopping after this cycle...";
    const std::string stp_cmd = "stop";

    do {
        std::getline(std::cin, input, '\n');
        run = !(input == stp_cmd);
        save = (input == "save");
        if (input == "smut")
        {
            std::cout << "Enter value (current: "<< g_smut << "): ";
            std::getline(std::cin, input, '\n');
            g_smut = std::stof(input);
        }
        if (input == "pmut")
        {
            std::cout << "Enter value (current: " << g_pmut << "): ";
            std::getline(std::cin, input, '\n');
            g_pmut = std::stof(input);
        }
        if (input == "mode")
        {
            std::cout << "Enter value (current: " << g_mode << ") [swap = 0; incdecins = 1]: ";
            std::getline(std::cin, input, '\n');
            g_mode = Mode(std::stoi(input));
        }

        if (input == "rin")
        {
            g_rin = true;
        }

        if (input == "help")
        {
            std::cout << "pmut          | display current pmut value and wait for new value input";
            std::cout << "smut          | display current smut value and wait for new value input";
            std::cout << "mode          | display current mode and wait for new mode input";
            std::cout << "rin           | manually reinitialize";
            std::cout << "stop          | quit programm safely (safes current progress if -e was given at start)";
        }

    } while (run);
    std::cout << "\033[1;31m" << stp_msg << "\033[0m\n" << std::endl;
}

int main(int argc, char** argv)
{
    if (argc <= 1)
    {
        printCommands();
    }
    else
    {
        std::string argv_ = argv[1];

        if (argv_ == "ga")
        {
            int i = 1;

            int pop = 10;
            int gen = 1000000;
            int size = -1;
            float mut = 0.005;
            float lim = 2.0;
            int rinDelay = 100;
            bool exportFile = false;
            bool rin = false;
            std::string importpath;

            while (argv[++i])
            {
                int id = -1;
                argv_ = argv[i];
                if (argv_ == "auto")
                {
                    std::thread t1(ga_compress_auto);
                    std::thread t2(input);
                    t1.join();
                    t2.join();
                    exit(0);
                }
                else if (argv_ == "-g" || argv_ == "-generations") id = 1;
                else if (argv_ == "-p" || argv_ == "-population") id = 2;
                else if (argv_ == "-s" || argv_ == "-size") id = 3;
                else if (argv_ == "-i" || argv_ == "-import") id = 4;
                else if (argv_ == "-pmut" || argv_ == "-primary_mutator") id = 5;
                else if (argv_ == "-l" || argv_ == "-limit") id = 6;
                else if (argv_ == "-rd" || argv_ == "-rinDelay") id = 7;
                else if (argv_ == "-smut" || argv_ == "-secondary_mutator") id = 8;
                else if (argv_ == "-r" || argv_ == "-rin") rin = true;
                else if (argv_ == "-e") exportFile = true;
                if (id == -1) continue;

                argv_ = argv[++i];
                switch (id)
                {
                case 1:
                    gen = std::stoi(argv_);
                    break;
                case 2:
                    pop = std::stoi(argv_);
                    break;
                case 3:
                    size = std::stoi(argv_);
                    break;
                case 4:
                    hist_in = argv_;
                    break;
                case 5:
                    mut = std::stof(argv_);
                    break;
                case 6:
                    lim = std::stof(argv_);
                    break;
                case 7:
                    rinDelay = std::stoi(argv_);
                    break;
                case 8:
                    g_smut = std::stof(argv_);
                    break;
                }
            }
            std::thread t1(ga_compress, pop, gen, size, mut, lim, rinDelay, rin, exportFile);
            std::thread t2(input);
            t1.join();
            t2.join();
        }
        else if (argv_ == "huffman")
        {
            argv_ = argv[2];
            if (argv_ == "benchmark")
            {
                if (argc >= 5)
                {
                    Benchmark(argv[3], argv[4]);
                }
                else
                {
                    std::cout << "Missing parameters, usage: input file, histogram" << std::endl;
                    exit(1);
                }
            }
            else if (argv_ == "encode")
            {
                if (argc >= 6)
                {
                    auto start = std::chrono::high_resolution_clock::now();
                    Huffman_Encode_Ga(argv[3], argv[4], argv[5]);
                    auto end = std::chrono::high_resolution_clock::now();
                    std::chrono::duration<double> elapsed = end - start;
                    std::cout << "Time: " << elapsed.count() << std::endl;
                }
                else if (argc >= 5)
                {
                    auto start = std::chrono::high_resolution_clock::now();
                    Huffman_Encode_Ga(argv[3], argv[4]);
                    auto end = std::chrono::high_resolution_clock::now();
                    std::chrono::duration<double> elapsed = end - start;
                    std::cout << "Time: " << elapsed.count() << std::endl;
                }
                else if (argc >= 4)
                {
                    auto start = std::chrono::high_resolution_clock::now();
                    Huffman_Encode_Ga(argv[3]);
                    auto end = std::chrono::high_resolution_clock::now();
                    std::chrono::duration<double> elapsed = end - start;
                    std::cout << "Time: " << elapsed.count() << std::endl;
                }
                else
                {
                    std::cout << "Missing parameters, usage: input file, histogram, output file" << std::endl;
                    exit(1);
                }
            }
            else if (argv_ == "decode")
            {
                if (argc >= 5)
                {
                    auto start = std::chrono::high_resolution_clock::now();
                    Huffman_Decode_Ga(argv[3], argv[4]);
                    auto end = std::chrono::high_resolution_clock::now();
                    std::chrono::duration<double> elapsed = end - start;
                    std::cout << "Time: " << elapsed.count() << std::endl;
                }
                else
                {
                    std::cout << "Missing parameters, usage: encoded file, output file" << std::endl;
                    exit(1);
                }
            }
            else if (argv_ == "both")
            {
                if (argc >= 7)
                {
                    auto start = std::chrono::high_resolution_clock::now();
                    Huffman_Encode_Ga(argv[3], argv[4], argv[5]);
                    auto end = std::chrono::high_resolution_clock::now();
                    std::chrono::duration<double> elapsed = end - start;
                    std::cout << "Encode time: " << elapsed.count() << std::endl;

                    start = std::chrono::high_resolution_clock::now();
                    Huffman_Decode_Ga(argv[5], argv[6]);
                    end = std::chrono::high_resolution_clock::now();
                    elapsed = end - start;
                    std::cout << "Decode time: " << elapsed.count() << std::endl;
                }
                else
                {
                    std::cout << "Missing parameters, usage: input file, histogram, output file = encoded file, output file" << std::endl;
                    exit(1);
                }
            }
        }
        else if (argv_ == "help")
        {
            printCommands("Displaying help");
        }
    }
}

void Benchmark(std::string filePath,  std::string hist)
{
    double fastest = -1;
    double slowest = -1;
    double avg = 0;

    std::vector<double> times;
    if (OPEN_IN(in, hist.c_str(), _O_RDONLY | _O_BINARY))
    {        
        //open failed
        perror("open failed on histogram file");
        exit(1);        
    }
    else
    {
        std::cout << "Benchmark genetic huffman encoding" << std::endl;
        //open succeeded
        if (OPEN_OUT(out, "huffman_ga_timings.txt", _O_WRONLY | _O_CREAT | _O_TRUNC))
        {
            perror("open failed on output file");
            exit(1);
        }

        for (int i = 0; i < 100; ++i)
        {
            auto start = std::chrono::high_resolution_clock::now();
            Huffman_Encode_Ga(filePath, hist, "encoded.huf");
            //Huffman_Decode_Ga("encoded.huf", "decoded.txt");
            auto end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> elapsed = end - start;
            times.push_back(elapsed.count());
        }
    }
    for (int i = 0; i < times.size(); ++i)
    {
        std::string line;
        line += std::to_string(i);
        line += ": ";
        line += std::to_string(times[i]);
        line += "\n";

        if (slowest == -1 || times[i] > slowest) slowest = times[i];
        if (fastest == -1 || times[i] < fastest) fastest = times[i];
        avg += times[i];

        const char* buf = line.c_str();

#if _WIN32 | _WIN64
        _write(out, buf, line.size());
#else
        write(out, buf, line.size());
#endif
    }

    std::string line;
    line += "\n";
    line += "Slowest: ";
    line += std::to_string(slowest);
    line += "\n";

    const char* bufs = line.c_str();
#if _WIN32 | _WIN64
    _write(out, bufs, line.size());
#else
    write(out, bufs, line.size());
#endif

    line = "";
    line += "Fastest: ";
    line += std::to_string(fastest);
    line += "\n";

    const char* buff = line.c_str();
#if _WIN32 | _WIN64
    _write(out, buff, line.size());
#else
    write(out, buff, line.size());
#endif

    line = "";
    line += "Average: ";
    line += std::to_string((avg/times.size()));
    line += "\n";

    const char* bufa = line.c_str();
#if _WIN32 | _WIN64
    _write(out, bufa, line.size());
#else
    write(out, bufa, line.size());
#endif
}

void printCommands(std::string msg)
{
    std::cout << msg << std::endl << std::endl;
    std::cout << "Commands:" << std::endl;
    std::cout << "      -> help             | display this message" << std::endl;
    std::cout << "      -> huffman          | genetic huffman encoding/decoding" << std::endl;
    std::cout << "         ---------------------------------------------------------------------------" << std::endl;
    std::cout << "          encode IN_FILEPATH HIST_FILEPATH OUT_FILEPATH" << std::endl;
    std::cout << "          decode IN_FILEPATH OUT_FILEPATH" << std::endl;
    std::cout << "          both IN_FILEPATH HIST_FILEPATH OUT_FILEPATH_ENCODE OUP_FILEPATH_DECODE" << std::endl;
    std::cout << "          benchmark IN_FILEPATH HIST_FILEPATH" << std::endl;
    std::cout << "         ===========================================================================" << std::endl;
    std::cout << "      -> ga               | evolutionary algorithm training" << std::endl;
    std::cout << "         ---------------------------------------------------------------------------" << std::endl;
    std::cout << "          -g INT          | number of generations" << std::endl;
    std::cout << "          -p INT          | size of population" << std::endl;
    std::cout << "          -s INT          | filesize in BYTE for webcrawler (-1 to disable)" << std::endl;
    std::cout << "          -i FILEPATH     | import .hist file to use" << std::endl;
    std::cout << "          -pmut FLOAT     | permutation factor for primary mutator" << std::endl;
    std::cout << "          -smut FLOAT     | permutation factor for secondary mutator" << std::endl;
    std::cout << "          -l FLOAT        | compression ratio limit for auto-exit" << std::endl;
    std::cout << "          -rd INT         | re-initialization delay in cycles" << std::endl;
    std::cout << "          -r              | enable auto-re-initialization" << std::endl;
    std::cout << "          -e              | enable export of .hist files" << std::endl;
    std::cout << "          --------------------------------------------------------------------------" << std::endl;
    std::cout << "          -auto           | autopilot mode, no other params needed (will be ignored)" << std::endl;
}

void ga_compress_auto()
{
    int pop = 100; 
    int gen = 1000000; 
    int size = 10485760; 
    float pmut = 0.0001; 
    float limit = 1.6; 
    int rinDelay = 500; 
    bool rin = true; 
    bool exp = true;

    if (OPEN_IN(in, "temp.txt", _O_RDONLY))
    {
        perror("open failed on input file: temp.txt");
        exit(1);
    }
    GA1DArrayGenome<uint64_t> genome(GA_LIMIT, Objective);

    genome.crossover(GA1DArrayGenome<uint64_t>::OnePointCrossover);
    genome.mutator(GA1DArrayGenome<uint64_t>::IncDecInsertMutator);             //added custom stuff

    genome.initializer(Initalizer);

    GASteadyStateGA ga(genome);
    ga.nGenerations(gen);
    ga.populationSize(pop);

    ga.pMutation(pmut);
    std::cout << "Initializing..." << std::endl;
    ga.initialize();

    hist_in = "";
    std::cout << "evolving for " << ga.nGenerations() << " generations" << std::endl;
    float scr = ga.statistics().bestIndividual().score();

    int cntr = 0;

    float l_smut = g_smut;
    g_pmut = pmut;

    //this is the main loop :)
    while (scr < limit && run) {
        genomeCounter = 0;
        switch (g_mode) 
        {
        case swap:
            ga.step();
            break;
        default:
            ga.step(l_smut);
            break;
        }
        
        
        if (ga.statistics().bestIndividual().score() > scr)
        {
            scr = ga.statistics().bestIndividual().score();
            ExportGenome(ga.statistics().bestIndividual());
            if (rin)
            {
                std::cout << "reinitializing" << std::endl;
                ga.initialize();
            }
            cntr = generationCounter;
        }
        float pM = ga.pMutation();
        std::cout << scr << " -- " << pM << " -- " << l_smut << " -- " << ++generationCounter  << std::endl;
        //pM *= 1.1;
        //ga.pMutation(pM);
        std::cout.flush();
        if (save) 
        {
            ExportGenome(ga.statistics().bestIndividual());
            save = false;
        }
        if ((cntr + rinDelay <= generationCounter && rin) || g_rin)
        {
            std::cout << "reinitializing" << std::endl;
            ga.initialize();
            cntr = generationCounter;
            g_rin = false;

            if (g_smut == 0.5)
            {
                g_smut = 0.25;
            }
            else if (g_smut == 0.25 && g_mode == incdecins)
            {
                g_mode = swap;
            }
            else if (g_mode == swap)
            {
                g_mode = incdecins;
                g_smut = 0.5;
            }
            
            if (scr > 1.4) g_pmut = 0.0000152;
        }
        if (g_smut != l_smut)
        {
            l_smut = g_smut;
            std::cout << "Changed smut to: " << l_smut << std::endl;
        }
        if (g_pmut != pM)
        {
            ga.pMutation(g_pmut);
            std::cout << "Changed pmut to: " << ga.pMutation() << std::endl;
        }
    }

    run = false;

abort:
    genome = ga.statistics().bestIndividual();

    std::cout << "Best fitness: " << genome.score() << std::endl;

    if (exp)
    {
        ExportGenome(genome);
    }
    exit(0);
}
void ga_compress(int pop, int gen, int size, float pmut, float limit, int rinDelay, bool rin, bool exp)
{
    if(size >= 0) webcrawler::GrabRandomWikipediaArticle_ToFile(std::to_string(size));
    if (OPEN_IN(in, "temp.txt", _O_RDONLY))
    {
        perror("open failed on input file: temp.txt");
        exit(1);
    }
    GA1DArrayGenome<uint64_t> genome(GA_LIMIT, Objective);

    genome.crossover(GA1DArrayGenome<uint64_t>::OnePointCrossover);
    genome.mutator(GA1DArrayGenome<uint64_t>::IncDecInsertMutator);             //added custom stuff

    genome.initializer(Initalizer);

    GASteadyStateGA ga(genome);
    ga.nGenerations(gen);
    ga.populationSize(pop);

    ga.pMutation(pmut);
    std::cout << "Initializing..." << std::endl;
    ga.initialize();

    hist_in = "";
    std::cout << "evolving for " << ga.nGenerations() << " generations" << std::endl;
    float scr = ga.statistics().bestIndividual().score();

    int cntr = 0;

    float l_smut = g_smut;
    g_pmut = pmut;

    //this is the main loop :)
    while (scr < limit && run) {
        genomeCounter = 0;
        switch (g_mode)
        {
        case swap:
            ga.step();
            break;
        default:
            ga.step(l_smut);
            break;
        }

        if (ga.statistics().bestIndividual().score() > scr)
        {
            scr = ga.statistics().bestIndividual().score();
            ExportGenome(ga.statistics().bestIndividual());
            if (rin)
            {
                std::cout << "reinitializing" << std::endl;
                ga.initialize();
            }
            cntr = generationCounter;
        }
        float pM = ga.pMutation();
        std::cout << scr << " -- " << pM << " -- " << l_smut << " -- " << ++generationCounter << std::endl;
        std::cout.flush();
        if (save)
        {
            ExportGenome(ga.statistics().bestIndividual());
            save = false;
        }
        if ((cntr + rinDelay <= generationCounter && rin) || g_rin)
        {
            std::cout << "reinitializing" << std::endl;
            ga.initialize();
            cntr = generationCounter;
            g_rin = false;
        }
        if (g_smut != l_smut)
        {
            l_smut = g_smut;
            std::cout << "Changed smut to: " << l_smut << std::endl;
        }
        if (g_pmut != pM)
        {
            ga.pMutation(g_pmut);
            std::cout << "Changed pmut to: " << ga.pMutation() << std::endl;
        }
    }

    run = false;

abort:
    genome = ga.statistics().bestIndividual();

    std::cout << "Best fitness: " << genome.score() << std::endl;

    if (exp)
    {
        ExportGenome(genome);
    }
    exit(0);
}

float Objective(GAGenome& g)
{
    ++genomeCounter;

    bool err_flag = false;
    int err_mem = 0;

    GA1DArrayGenome<uint64_t>& arr = (GA1DArrayGenome<uint64_t>&)g;

    uint64_t* arr_ptr = (uint64_t *) &arr.gene(0);
retry:

    std::filesystem::create_directories(trainingPath);

    std::string outPath = trainingPath + "/encoded_" + std::to_string(genomeCounter) + ".huf";
    int err_c = 0;

#if _WIN32 | _WIN64
    _lseek(in, 0L, SEEK_SET);
    off_t input_size = _lseek(in, 0L, SEEK_END);
    _lseek(in, 0L, SEEK_SET);   //reset pointer to start
#else
    lseek(in, 0L, SEEK_SET);
    off_t input_size = lseek(in, 0L, SEEK_END);
    lseek(in, 0L, SEEK_SET);   //reset pointer to start
#endif

    if (OPEN_OUT(out, outPath.c_str(), _O_WRONLY | _O_BINARY | _O_CREAT | _O_TRUNC | _O_TEMPORARY))      //added _O_TEMPORARY to keep HDD empty
    {
        std::cout << "Failed to open temp output file: " << outPath.c_str() << std::endl;
        exit(1);
    }

    huf_config_t config = {
       input_size,
       0,
       HUF_128KIB_BUFFER,
       HUF_128KIB_BUFFER,
       in,
       out,
    };

    huf_histogram_t* hist = new huf_histogram_t;

    hist->iota = 1;
    hist->length = arr.length();
    hist->start = 0;
    hist->end = 0;

    hist->frequencies = reinterpret_cast<uint64_t*>(calloc((uint64_t)arr.length() * 2, sizeof(uint64_t)));

    if (hist->frequencies == nullptr) exit(1);
    memcpy(hist->frequencies, arr_ptr, arr.length() * sizeof(uint64_t));

    size_t len = 0;

    if (err_flag == true && hist->frequencies[err_mem] == 0)
    {
        std::cout << "Why is this 0?("<< err_mem << ") | " << arr.gene(err_mem);
        arr_ptr[err_mem] = 1;
        std::cout << "Why is this 0?" << " | " << arr.gene(err_mem);
    }

    huf_error_t err = huf_encode_ga(&config, hist, &len);

    if (err != HUF_ERROR_SUCCESS)
    {
        if (err == err_mem)
        {
            err_flag = true;
            std::cout << char(err) << std::endl;
        }
        err_mem = err;
        arr_ptr[err] = 1;

#if _WIN32 | _WIN64
        _close(out);
#else
        close(out);
#endif
        goto retry;
    }
#if _WIN32 | _WIN64
    _lseek(out, 0L, SEEK_SET);

    long ret = _lseek(out, 0L, SEEK_END);

    ret -= len;

    _lseek(in, 0L, SEEK_SET);
#else
    lseek(out, 0L, SEEK_SET);

    long ret = lseek(out, 0L, SEEK_END);

    ret -= len;

    lseek(in, 0L, SEEK_SET);
#endif

    err_c = 0;
#if _WIN32 | _WIN64
    _close(out);
#else
    close(out);
#endif

    return ((float)input_size / (float)ret);
}


void Initalizer(GAGenome& c)
{
    GA1DArrayGenome<uint64_t>& arr = (GA1DArrayGenome<uint64_t>&)c;

    int loc_in;

    if (OPEN_IN(loc_in, hist_in.c_str(), _O_RDONLY | _O_BINARY))
    {
        for (int i = 0; i < 256; ++i)
        {
            int p = rand() % 20 + 1;
            arr.gene(i, p);
        }
    }
    else
    {
        uint64_t p;
        for (int i = 0; i < GA_LIMIT; ++i)
        {
#if _WIN32 | _WIN64
            _read(loc_in, &p, sizeof(uint64_t));
#else
            read(loc_in, &p, sizeof(uint64_t));
#endif
            arr.gene(i, p);
            
        }
        //hist_in = "";

#if _WIN32 | _WIN64
        _close(loc_in);
#else
        close(loc_in);
#endif
    }
}

void Huffman_Encode_Ga(std::string filePath, std::string histPath, std::string outPath)
{
    std::cout << "Testfunction for huffman algorithm genetic encoding" << std::endl;

    #if _WIN32 | _WIN64
        if (filePath == "") filePath = openfilename();
        std::cout << "Obtained input path: " << filePath << std::endl;
        if (histPath == "") histPath = openfilename((LPWSTR)(L"Histogram Files (*.hist)\0*.hist\0"), NULL);
        std::cout << "Obtained histogram path: " << histPath << std::endl;
    #endif

    int in, in2, out;

    if (OPEN_IN(in2, histPath.c_str(), _O_RDONLY | _O_BINARY))
    {
        perror("open failed on histogram file");
        exit(1);
    }
    uint64_t* temp = reinterpret_cast<uint64_t*>(calloc(GA_LIMIT, sizeof(uint64_t)));
    uint64_t element = 0;


    int i = 0;
    if (temp == nullptr) exit(1);
#if _WIN32 | _WIN64
    while(_read(in2, &element, sizeof(uint64_t)))
    {
        temp[i] = element;
        ++i;
    }

#else
    while (read(in2, &element, sizeof(uint64_t)))
    {
        temp[i] = element;
        ++i;
    }
#endif

retry_enc:
    if (OPEN_IN(in, filePath.c_str(), _O_RDONLY))
    {
        perror("open failed on input file");
        exit(1);
    }

    huf_histogram_t* hist = new huf_histogram_t;

    hist->iota = 1;
    
    hist->start = 0;
    hist->end = GA_LIMIT;
    hist->frequencies = reinterpret_cast<uint64_t*>(calloc(GA_LIMIT * 2, sizeof(uint64_t)));
    
    if (hist->frequencies == nullptr || temp == nullptr) exit(1);
    memcpy(hist->frequencies, temp, GA_LIMIT * sizeof(uint64_t));
    

    hist->length = GA_LIMIT;
#if _WIN32 | _WIN64
    off_t input_size = _lseek(in, 0L, SEEK_END);
    _lseek(in, 0L, SEEK_SET);   //reset pointer to start
#else
    off_t input_size = lseek(in, 0L, SEEK_END);
    lseek(in, 0L, SEEK_SET);   //reset pointer to start
#endif

    if (OPEN_OUT(out, outPath.c_str(), _O_WRONLY | _O_BINARY | _O_CREAT | _O_TRUNC))
    {
        perror("open failed on output file");
        exit(1);
    }

    huf_config_t config = {
       input_size,
       0,
       HUF_128KIB_BUFFER,
       HUF_128KIB_BUFFER,
       in,
       out,
    };

    size_t len = 0;

    huf_error_t err = huf_encode_ga(&config, hist, &len);

    if (err != HUF_ERROR_SUCCESS)
    {
#if _WIN32 | WIN64
        _close(in);
        _close(out);
#else
        close(in);
        close(out);
#endif
        std::cout << "missing char: " << char(err) << std::endl;
        temp[(unsigned char)err] = 1;
        goto retry_enc;
        exit(err);
    }
#if _WIN32 | WIN64
    _lseek(out, 0L, SEEK_SET);

    long ret = _lseek(out, 0L, SEEK_END);

    _lseek(in, 0L, SEEK_SET);
#else
    lseek(out, 0L, SEEK_SET);

    long ret = lseek(out, 0L, SEEK_END);

    lseek(in, 0L, SEEK_SET);
#endif

    std::cout << "Compression ratio: " << ((double)input_size / double(ret)) << std::endl;
    std::cout << "Input size: " << input_size << std::endl;
    std::cout << "Size in Bytes: " << ret << std::endl;
#if _WIN32 | WIN64
    _close(in);
    _close(in2);
    _close(out);
#else
    close(in);
    close(in2);
    close(out);
#endif
}

void Huffman_Decode_Ga(std::string inPath, std::string outPath)
{
    std::cout << "Testfunction for huffman algorithm genetic decoding" << std::endl;

    int in, out;

    if (OPEN_IN(in, "encoded.huf", _O_RDONLY | _O_BINARY))
    {
        perror("open failed on input file");
        exit(1);
    }
#if _WIN32 | WIN64
    off_t input_size = _lseek(in, 0L, SEEK_END);
    _lseek(in, 0L, SEEK_SET);   //reset pointer to start
#else
    off_t input_size = lseek(in, 0L, SEEK_END);
    lseek(in, 0L, SEEK_SET);   //reset pointer to start
#endif

    if (OPEN_OUT(out, "decoded.txt", _O_WRONLY | _O_CREAT | _O_BINARY | _O_TRUNC))
    {
        perror("open failed on output file");
        exit(1);
    }

    huf_config_t config = {
       input_size,
       0,
       HUF_128KIB_BUFFER,
       HUF_128KIB_BUFFER,
       in,
       out,
    };

    huf_error_t err = huf_decode_ga(&config);
#if _WIN32 | WIN64
    _close(in);
    _close(out);
#else
    close(in);
    close(out);
#endif
}

void ExportGenome(const GAGenome& g)
{
    GA1DArrayGenome<uint64_t>& arr = (GA1DArrayGenome<uint64_t>&)g;

    std::filesystem::path path = get_current_dir();	//if no path is given use a default one

    path += "/exports";

    std::filesystem::create_directories(path);

    time_t now = time(0);
    tm ltm;
    #if _WIN32 | WIN64
    localtime_s(&ltm, &now);
    #else
    localtime_r(&now, &ltm);
    #endif

    float scr = g.score();

    std::string dateString = std::to_string(1900 + ltm.tm_year);
    dateString += std::to_string(1 + ltm.tm_mon);
    dateString += std::to_string(ltm.tm_mday) + "_" + std::to_string(ltm.tm_hour) + std::to_string(ltm.tm_min) + std::to_string(ltm.tm_sec);

    std::string outPath = "./exports/";

    outPath += "best_" + dateString + "_" + std::to_string(scr) + ".hist";

    int out;


    if (OPEN_OUT(out, outPath.c_str(), _O_WRONLY | _O_BINARY | _O_CREAT))       //file protected on purpose
    {
        std::string errMsg = "open failed on output file" + outPath;
        perror(errMsg.c_str());
        exit(1);
    }
    else
    {
        for (int i = 0; i < arr.length(); ++i)
        {
#if _WIN32 | _WIN64 
            _write(out, &arr[i], sizeof(uint64_t));
        }
        _close(out);
#else
            write(out, &arr[i], sizeof(uint64_t));
        }
        close(out);
#endif
    }
    
    hist_in = outPath;

    std::cout << "Saved " << outPath << std::endl;
}

void ExportGenome(std::string histPath, uint64_t* arr) 
{
    
    int newout;

    if (OPEN_OUT(newout, histPath.c_str(), _O_WRONLY | _O_BINARY | _O_CREAT))
    {
        perror("open failed on output file");
        exit(1);
    }
    else
    {

        for (int i = 0; i < GA_LIMIT; ++i)
        {
#if _WIN32 | _WIN64 
            _write(newout, &arr[i], sizeof(uint64_t));
#else
            write(newout, &arr[i], sizeof(uint64_t));
#endif
        }
    }
}

static std::filesystem::path get_current_dir() {
    char buff[FILENAME_MAX]; //create string buffer to hold path
    #if _WIN32 | _WIN64 
    _getcwd(buff, FILENAME_MAX);
    #else
    getcwd(buff, FILENAME_MAX);
    #endif
    std::filesystem::path current_working_dir(buff);
    return current_working_dir;
}

