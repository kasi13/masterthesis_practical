import requests, sys


url = 'https://en.wikipedia.org/wiki/Special:Random' 

response = requests.get(url, allow_redirects=True)

temp = response.url

temp = temp.split("/wiki/")

url = temp[0] + "/w/index.php?title=" + temp[1] + "&action=edit"

response = requests.get(url)

temp = response.text.split('<textarea aria-label="Wikitext source editor" tabindex="1" accesskey="," id="wpTextbox1" cols="80" rows="25" style="" class="mw-editfont-monospace" lang="en" dir="ltr" name="wpTextbox1">')

text = temp[1].split("</textarea>")[0]

f = open("temp.txt", "w", encoding='utf-8')

f.write(text)

size = len(text)	#in byte
text = "\n"

i = 0

while(size < int(sys.argv[1])): #=> 1MB = 1048576 Bytes
	try:
		i = i+1
		print("Request #" + str(i) + ": " + str(size) + " Byte")
		url = 'https://en.wikipedia.org/wiki/Special:Random' 
		response = requests.get(url, allow_redirects=True)
		temp = response.url
		temp = temp.split("/wiki/")
		url = temp[0] + "/w/index.php?title=" + temp[1] + "&action=edit"

		response = requests.get(url)

		temp = response.text.split('<textarea aria-label="Wikitext source editor" tabindex="1" accesskey="," id="wpTextbox1" cols="80" rows="25" style="" class="mw-editfont-monospace" lang="en" dir="ltr" name="wpTextbox1">')

		text += temp[1].split("</textarea>")[0]
		size += len(text)
		f.write(text)
		text = "\n"
	except Exception as e:
		print(e)
f.close()

print("Grabbed a text from wikipedia size: " + str(size))